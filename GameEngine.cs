﻿using System;
namespace GameTools
{

    /*
     * GameEngine:
     * Protype to print sequence of frames in console.
     * 
     * To stop de main while press ESC key.
     * 
     */


    public class GameEngine
    {

        //Declaració d'una variable
        private ConsoleColor _backgroundConsoleColor;
        //Declaració d'una propietat
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                //Ternari condition
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }


        //Declaració d'una propietat no protegida
        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }


        private void InitGame()
        {
            /*** Init variables ***/

            Frames = 0;
            engineSwitch = false;

            //Acces exemple with this:
            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;

            //Calculate the frame time in miliseconds. Time to refresh. F=1/s ->s=1/F
            time2liveframe = (int)((1 / _frameRate) * 1000);

            /*******/

            //Prepare Console
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;

            Console.WriteLine("\nPress a key to display; press the ESC key to quit.");

            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");

            Start();

            System.Threading.Thread.Sleep(2000);
        }

        /*
         * Engine updates every frame
         * 
         * Reprint console
         */
        private void UpdateGame()
        {

            do
            {

                while (Console.KeyAvailable == false)
                {

                    CleanFrame();

                    Update();

                    CheckKeyboard4Engine();

                    Console.WriteLine(engineSwitch);

                    RefreshFrame();

                    Frames++;
                }

                cki = Console.ReadKey(true);

            } while (engineSwitch);

        }

        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key == ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {

            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit();
            Console.WriteLine(" Game Over. Closing game");
        }


        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop + 1);
        }

        private static char[,] matriu = new char[9, 9];
        private static int longFila = matriu.GetLength(0);
        private static int longColumna = matriu.GetLength(1);

        protected void Start()
        {
            //Code before first frame

            Random random = new Random();

            Console.WriteLine("Printing Matrix: ");
            for (int i = 0; i < longFila; i++)
            {

                for (int j = 0; j < longColumna; j++)
                {

                    //matriu[i, j] = (char)random.Next(33, 120);
                    matriu[i, j] = '0';
                    Console.Write(matriu[i, j] + "\t");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }

        protected void Update()
        {
            //Execution ontime secuence of every frame
            Random random = new Random();
            int columna = random.Next(0, 8);
            char lletra = (char)random.Next(33, 120);
            matriu[0, columna] = lletra;

            for (int i = 0; i < longFila; i++)
            {
                for (int j = 0; j < longColumna; j++)
                { 
                    if (j == columna && i > 0 && i < (longColumna-1))
                    {
                        matriu[i-1, j] = (char)'0';
                        matriu[i, j] = lletra;
                    }
                    Console.Write(matriu[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }

        protected void Exit()
        {
            //Code afer last frame
            Console.WriteLine("Printing Matrix: ");
            for (int i = 0; i < longFila; i++)
            {
                for (int j = 0; j < longColumna; j++)
                {
                    Console.Write(matriu[i, j] + "\t");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }

    }
}


public class ConsoleSpiner
{
    int counter;
    public ConsoleSpiner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
        //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
}
